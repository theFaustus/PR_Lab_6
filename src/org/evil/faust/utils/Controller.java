package org.evil.faust.utils;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextArea;
import javafx.stage.FileChooser;

import java.io.File;

public class Controller {
    @FXML
    public TextArea fileTypesTextArea;
    private File file;

    @FXML
    public void onOpenFileButton(ActionEvent actionEvent){
        file = getFile();
        if(file != null) {
            FileTypeDetector fileTypeDetector = new FileTypeDetector(file);
            String detectedFormats = fileTypeDetector.detectInnerFileType();
            fileTypesTextArea.clear();
            if(detectedFormats.isEmpty())
                detectedFormats = "File format not found.";
            fileTypesTextArea.setText(detectedFormats);
        } else {
            alertThatFileNotLoaded();
        }
    }

    @FXML
    public void alertThatFileNotLoaded() {
        Alert alert = new Alert(Alert.AlertType.ERROR, "Oops, looks like something went wrong.\nWe couldn`t open your file.", ButtonType.CLOSE);
        alert.setHeaderText("Not loaded");
        alert.setTitle("Error Dialog");
        alert.showAndWait();
    }

    private File getFile() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Choose attachment");
        fileChooser.getExtensionFilters()
                .addAll(new FileChooser.ExtensionFilter("Files", "*.*")); // files
        return fileChooser.showOpenDialog(null);
    }
}

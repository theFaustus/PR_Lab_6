package org.evil.faust.utils;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Faust on 5/6/2017.
 */
public class FileTypeDetector {
    private File file;
    private String innerFileType = "";
    private Map<String, String> formatInHEX = new HashMap<>();
    private Map<String, String> formatInString = new HashMap<>();
    private static int i = 0;

    public FileTypeDetector(File file) {
        this.file = file;
        formatInHEX.put("^(0x)?(ffd8ff|ffd8ffd8)", ".jpg");
        formatInHEX.put("^(0x)?(89504e47)", ".png");
        formatInHEX.put("^(0x)?(424d)", ".bmp");
        formatInHEX.put("^(0x)?(47494638)", ".gif");
        formatInHEX.put("^(0x)?(4949)", ".tiff");
        formatInHEX.put("^(0x)?(38425053)", ".psd");
        formatInHEX.put("^(0x)?(494433)", ".mp3");
        formatInHEX.put("^(0x)?(52494646)", ".avi");
        formatInHEX.put("^(0x)?(465753)", ".swf");
        formatInHEX.put("^(0x)?(d0cf11e0a1b11ae1)", ".formats based on zip");
        formatInHEX.put("^(0x)?(526172211a0700)", ".rar");
        formatInHEX.put("^(0x)?(4d5a)", ".exe");
        formatInHEX.put("^(0x)?(25504446)", ".pdf");

        formatInString.put("(..JFIF..|..Exif..|ÿØÿÛ)", ".jpg");
        formatInString.put("^(.PNG....|.PNG.|.PNG)", ".png");
        formatInString.put("^(BM)", ".bmp");
        formatInString.put("^(GIF87a|GIF89a)", ".gif");
        formatInString.put("^(MM.*)", ".tiff");
        formatInString.put("^(8BPS)", ".psd");
        formatInString.put("^(ID3|ÿû)", ".mp3");
        formatInString.put("^(RIFF....AVI.)", ".avi");
        formatInString.put("^(.CWS|EWS)", ".swf");
        formatInString.put("^(PK..)", ".formats based on zip");
        formatInString.put("^(Rar!...)", ".rar");
        formatInString.put("^(MZ)", ".exe");
        formatInString.put("^(%PDF)", ".pdf");
    }

    public String detectInnerFileType() {
        InputStream is = null;
        BufferedReader buf = null;
        i = 0;
        try {
            is = new FileInputStream(file);
            buf = new BufferedReader(new InputStreamReader(is));
            String line = buf.readLine();

            while (line != null) {
                innerFileType += searchFileTypeInStream(line);
                line = buf.readLine();
            }
            if(innerFileType.isEmpty()){
                is = new FileInputStream(file);
                buf = new BufferedReader(new InputStreamReader(is));
                line = buf.readLine();
                while (line != null) {
                    innerFileType += searchFileTypeInDocument(line);
                    line = buf.readLine();
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return innerFileType;
    }

    public String searchFileTypeInStream(String content){
        String result = "";
        String foundBytes = "";

        for(Map.Entry<String, String> e : formatInHEX.entrySet()) {
            Pattern pattern = Pattern.compile(e.getKey());
            Matcher matcher = pattern.matcher(content);
            while (matcher.find()){
                foundBytes = matcher.group(2);
                if(foundBytes != null){
                    result = "File format #" + ++i + " = " + e.getValue() + (i == 0 ? "" : "\n");
                }
            }
        }

        return result;
    }

    public String searchFileTypeInDocument(String content){
        String result = "";
        String foundBytes = "";

        for(Map.Entry<String, String> e : formatInString.entrySet()) {
            Pattern pattern = Pattern.compile(e.getKey());
            Matcher matcher = pattern.matcher(content);
            while (matcher.find()){
                foundBytes = matcher.group(1);
                if(foundBytes != null){
                    result = "File format #" + ++i + " = " + e.getValue() + (i == 0 ? "" : "\n");
                }
            }
        }
        return result;
    }
}
